2023-02-05 Mike Gabriel

        * Release 0.1.1 (HEAD -> main, tag: 0.1.1)

2022-10-04 Mike Gabriel

        * Merge branch 'fix-spesific-typo' into 'main' (90cac0d)

2022-10-04 Jami Kettunen

        * Fix help "spesific" -> "specific" typo (5cab17a)

2022-07-14 Ratchanan Srirattanamet

        * Merge branch 'empty-halium-yaml' into 'main' (8483ed6)

2022-07-08 Alfred Neumayer

        * devices: Ship empty Halium overlay target file (501d133)

2021-08-16 Dalton Durst

        * Merge branch 'revert-020fd7ed' into 'main' (aec1333)
        * Revert "Merge branch 'wouter182-main-patch-72613' into 'main'"
          (2cad801)

2021-08-10 Dalton Durst

        * Name PineTab device correctly (e8ca13b)
        * Merge branch 'wouter182-main-patch-72613' into 'main' (020fd7e)

2021-08-08 wouter182

        * Update pinephone.conf Closely tight to
          https://github.com/ubports/sensorfw/pull/4 Cleanup some
          settings which don't work. (b0c8450)

2021-08-03 Dalton Durst

        * Merge branch 'personal/usb/pinetab-fun' into 'main' (f0d2f92)

2021-07-29 Dalton Durst

        * Enable accelerometer on PineTab (fcfe94b)
        * Fix PineTab config with new kernel (7931b04)

2021-04-07 Dalton Durst

        * Merge branch 'ubports/xenial_-_edge' into 'main' (d47492b)

2021-04-06 Marius Gripsgard

        * platform: Fix wrong defined name in previous commit (8cb0ecb)

2021-04-01 Marius Gripsgard

        * device: Search for device configs using case insensitive matching
          (and tests) (6927ca2)
        * utils: Add compareInsensitive function and tests (ebe1e10)
        * tests: Add tests for ends- and starts- with (8479319)

2021-04-01 Dalton Durst

        * Add libgtest-dev to build-depends (0d51430)

2021-03-29 Marius Gripsgard

        * Move Jenkinsfile to debian folder (65348c9)
        * Merge pull request #11 from z3ntu/xenial_-_edge_-_cmake-config-path
          (47255cc)
        * Merge pull request #12 from ubports/xenial_-_edge_-_yaml (a55aad2)

2021-03-17 Marius Gripsgard

        * Merge branch 'mr/device-info-manpage' into 'master' (3bdd6b1)
        * Merge branch 'mr/fix-build-without-androidprops' into 'master'
          (7203468)

2021-03-14 Mike Gabriel

        * debian/deviceinfo-tools.manpages: Install manpage into -tools
          package. (62429dd)
        * tools/: Add rudimentary device-info man page. (cf2b7d7)
        * src/platform/platform.cpp: Fix FTBFS when building without Android
          properties. (c25eedf)

2021-03-12 Marius Gripsgard

        * tests: Add tests for utils (6ffc1b6)
        * tests: Use older MOCK_METHOD macro style (748ff41)

2021-02-03 Marius Gripsgard

        * Add more tests (db3278c) (tag: v0.1.0)
        * Add more tests, coverage raports and fix legacyprops (02d4a52)

2021-02-01 Marius Gripsgard

        * Move to yaml for config files (b7de280)

2020-08-17 Marius Gripsgard

        * Inital tests (e270414)

2020-09-01 Marius Gripsgard

        * Refactor platform dependent information getter logic (304a925)

2020-09-16 Luca Weiss

        * Allow changing of CONFIG_PATH during compile-time (e3dd087)

2020-09-04 Marius Gripsgard

        * Don't hardcode config directory and rename default config dir to
          match project (#9) (a830f33)

2020-08-17 Marius Gripsgard

        * Inital tests (2292809)

2020-08-06 Marius Gripsgard

        * Add alias for pinephone board v1.2 (06e02d4)

2020-06-11 Marius Gripsgard

        * Merge pull request #5 from erfanoabdi/xenial_-_edge (544833a)

2020-06-11 Erfan Abdi

        * Use vendor props if available (a494268)

2020-04-14 Marius Gripsgard

        * Add newer pinephone alias (93ac032)

2020-04-04 Marius Gripsgard

        * Fix prox threshold (97036dc)

2020-03-21 Marius Gripsgard

        * Fix derp (291573e)

2020-03-09 Marius Gripsgard

        * Add pinephone sensorfw configs (d30458b)

2020-03-03 Michele

        * fix typo (3333598)

2020-02-08 Dalton Durst

        * Make scaling smaller on Pinephone (a2f5590)

2019-12-13 Marius Gripsgard

        * Pinephone should be in Portrait (1d15431)

2019-12-08 Marius Gripsgard

        * default should be Landscape on desktop and Portrait on halium
          (360d81b)

2019-11-13 Marius Gripsgard

        * Fix derp (c85cda2)

2019-11-05 Marius Gripsgard

        * Update to new jenkinsfile (a07797f)

2019-11-03 Marius Gripsgard

        * Add sensorfw config, move folder and fix gridunit on pine/librem
          (bcfc13d)

2019-09-20 Marius Gripsgard

        * Add configs for pine(book/tab) and librem5 (6de9c93)
        * Turn detectedName std::string into cont char* to "normalify it"
          (8df2516)
        * Build depend on pkg-config (2e9e5f5)
        * Add "about this device" option to the tool (36a30f9)
        * Add api point to set printmode after construction (0b00336)
        * Add ability to read legacy config files (39a594b)
        * Give default gridunit if cant convert string to int (7a27663)
        * Add error logger (f404eb4)
        * Cleanup data and add alias (6019348)

2019-09-19 Marius Gripsgard

        * Add support for alias, logging and general improvements (0a8e12f)

2019-09-18 Marius Gripsgard

        * Make sure -dev depends on its libs (d4bbbc9)
        * Seperate supportedOrientations into vector (4d9c858)
        * Dont write anything on error, scripts wont like this (e519694)
        * [tool] Add help and move get under get subcommand (e29252e)
        * Add WebkitDpr to config and edit PrimaryOr to reflect native Or
          (0a68281)

2019-09-17 Marius Gripsgard

        * Merge branch 'master' into xenial_-_edge (cb09574)
        * Add jenkinsfile (922f8fc)
        * Inital commit (4539116)
        * Create README.md (d162457)
        * Create LICENSE (edf81f4)
